unit uTranslationStrings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
  // All the translation strings that Lazarus doesn't create automatically
  i18nLoadFile = 'Open file';
  i18nReplace = 'Replace';
  i18nKeep = 'Keep';
  i18nCancel = 'Cancel';
  i18nConfirm = 'Confirm';
  i18nRootfileLoaded = 'Root file loaded';
  i18nSuccess = 'Success';
  i18nError = 'Error';
  i18nFileDoesntExist = 'File does not exist';
  i18nFolderDoesntExist = 'Folder does not exist';
  i18nNoFileSelected = 'No file selected!';
  i18nLoadRoot = 'Load root file?';
  i18nLoadRootDesc = 'Do you want to load the selected root file?';
  i18nBuildFiles = 'Build files?';
  i18nBuildFilesDesc = 'Do you want to build the selected files?';
  i18nSelectOutputDir = 'Choose output folder?';
  i18nSelectOutputDirDesc = 'Do you really want to choose this output folder?';
  i18nNoRootOrFolder = 'You have to choose a root file and an output folder!';
  i18nFinished = 'All files built successfully!';
  i18nChooseFolder = 'Choose an output folder';
  i18nDirSelected = 'Output folder selected';
  i18nUnsupportedFileType = 'Nicht unterstützter Dateityp!';

implementation

end.

