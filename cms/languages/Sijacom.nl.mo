��    :      �  O   �      �  �   �     �       	   %     /     <  #   I  %   m     �     �     �     �     �  	   �     �  
   �  0   �  (   -  +   V     �     �     �     �     �     �     �     �     �     �     �     �                       	   2     <     S  	   b     l     s     x     �  
   �     �  
   �     �     �     �     �  4   �     		  4    	  ,   U	  1   �	  )   �	     �	  {  �	  ,  z     �  !   �  	   �     �     �       )   $  	   N     X     l     {     �     �  
   �     �  #   �  )   �  ,     
   4     ?     F     K     P     d     l     �     �     �     �     �     �     �     �     �     �     �       	        )     /     4     <     N     \     h     w          �     �  2   �  
   �     �     �  	   �  	   �  
        $      3          *             -   :      7   6   /   %   '      #                                  4       !   "                 5      1      &                           (          +   ,          .   9   0                 )   
       	   2                                  8        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 About Sijacom All files built successfully! Build all Build files: Build files? C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language Load root file? Log No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo PO-Revision-Date: 2021-08-26 15:02+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Dutch <https://weblate.bubu1.eu/projects/sijacom/sijacom/nl/>
Language: nl
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Eenvoudig samenvoegen en content management" is 
een eenvoudige statische website generator die als doel 
heeft het vereenvoudigen van het proces van het 
aanpassen van statische website elementen over 
meerdere sub-pagina's. Raadpleeg de handleiding voor 
informatie over de bediening van Sijacom.
 Over Sijacom Alle bestanden succesvol gebouwd! Bouw alle Bouw bestanden: Bestanden opbouwen? C:\documentenMijngrappigProject C:\documents\MijnGrappigProject\root.json Annuleren Kies een uitvoermap Kies directory Kies output map? Sluiten Compilator: Bevestigen Ontwikkelaar: Wilt u echt deze uitvoermap kiezen? Wilt u de geselecteerde bestanden bouwen? Wilt u het geselecteerde basisbestand laden? Nederlands Engels Fout Ests Fatcow Pictogrammen Bestand Bestand bestaat niet De map bestaat niet Free Pascal Compiler Duits Iconen: Houd Taal Laad root file? Loog Geen bestand gekozen! Open bestand Uitvoermap geselecteerd Uitvoer map: Pixelcode Pools Nav. Vervang Root file geladen Root bestand: SOS stoppen Selecteer alle Sijacom Slowaaks Succes Alles deselecteren Je moet een hoofdbestand en een uitvoermap kiezen! Informatie Kies... Kies... Selecteer Selecteer Informatie 