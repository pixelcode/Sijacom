��    I      d  a   �      0  �   1     1     3     A  	   _     i     v  	   �  #   �  %   �     �     �     �            	   #     -     5  
   <  0   G  (   x  +   �     �     �     �     �     �     �     �     	     )	     1	     G	     \	  	   c	  	   m	     w	     ~	     �	     �	     �	     �	  	   �	     �	  	   �	     �	     �	  	   �	     �	     
     	
     
     
  
   +
     6
  
   ?
     J
     R
  	   Y
     c
     k
     s
     �
     �
  4   �
     �
  4   �
  ,   +  1   X  )   �     �     �    �  1  Y     �     �     �     �     �     �     �  *   �  ,      	   M     W     u     �  	   �     �     �     �     �  8   �  2      5   S     �     �     �     �     �     �     �     �     �     �                 	   %     /     7     ?     E     L     h  	   o     y     �     �     �  	   �     �     �  	   �     �     �               +     ?  
   G     R     [     c      i     �     �  A   �     �     �     
          %     2     8        4                   B       ;       3   $   I         8   0      A   -   C           
   F   (          :      @   	       ,            <   2         &              "   '           >   *           E                         /   H   6         =   #                  7   ?   G   1      )                       D          5   .   !   %   +          9        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Hungarian Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Romanian Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-29 15:10+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Hungarian <https://weblate.bubu1.eu/projects/sijacom/sijacom/hu/>
Language: hu
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 Az "Simple Joining And COntent Management" egy 
egyszerű statikus weboldal generátor, amelynek célja, 
hogy leegyszerűsítse a a statikus weboldal elemek 
beállításának folyamatát több aloldalon. Kérjük, olvassa 
el a kézikönyvet a a Sijacom működtetésével 
kapcsolatos információkat.
 , A Sijacomról Minden fájl sikeresen épült! Építsd az összes Build fájlok: Build fájlok? Bolgár C:\dokumentumok\AzénViccesProjekt\output\ C:\dokumentumok\AzénViccesProjekt\root.json Törölje Kimeneti mappa kiválasztása Válasszon könyvtárat Kimeneti mappa kiválasztása? Bezárás Összeállító: Megerősítés Dán Fejlesztő: Tényleg ezt a kimeneti mappát szeretné kiválasztani? Szeretné elkészíteni a kiválasztott fájlokat? Be kívánja tölteni a kiválasztott gyökérfájlt? Holland Angol Hiba Észt Fatcow ikonok Fájl Nem létezik a fájl Nem támogatott fájltípus! Finn Nem létezik a mappa Free Pascal Compiler Német Magyar Izlandiak Ikonok: Tovább Nyelv LiJu09 Gyökér fájl betöltése? Napló Mondstern Nincs kijelölt fájl! Megnyílt fájl Kimeneti mappa kiválasztva Kimeneti mappa: Pixelcode Lengyel Ht. Cserélje Román Betöltött gyökérfájl Gyökér fájl: SOS megálló Válassza ki mindet Sijacom Szlovákia Szlovén Sikeres Svéd Sijacom fordítása a Weblate-on Fordítók: Összes kijelölés törlése Ki kell választania egy gyökérfájlt és egy kimeneti mappát! Infó Válasszon... Válasszon... Válassza ki Válassza ki Infó v1.1 