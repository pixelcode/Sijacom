# Sijacom

Sijacom stands for: Simple Joining and COntent Management. It's a tiny static website generator. 

# Screenshots

|Main window|Building protocol|
|---|---|
|![](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/src/screenshots/Main%20window%20example%20screenshot.png)|![](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/src/screenshots/Log%20window%20screenshot.png)|

# How to download Sijacom

## Install Sijacom on Windows

If you're on Windows, installing Sijacom works like this:

1. Download [Sijacom-setup.exe](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/installation/Sijacom-setup.exe)
2. Run it and follow the installation wizard.
3. Done!

## Portable version for Windows

1. Download [Sijacom-portable.zip](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/installation/Sijacom-portable.zip)
2. Extract it into a directory of your choice.
3. Run Sijacom.exe
4. Done!

Note that without installing there won't be an association of Sijacom and the .sijacom file type. That means that you won't be able to open root files with Sijacom by double-clicking on them.

## Compile Sijacom

If you're not on Windows, do the following:

1. Download and install the [Lazarus IDE](https://www.lazarus-ide.org/index.php?page=downloads).
2. Download the entire Sijacom repository (there's a download button on [Codeberg](https://codeberg.org/pixelcode/Sijacom)).
3. Open `/cms/Sijacom.lpi` in Lazarus.
4. Click on the green compile button in the top left or hit F9.
5. Done!

## Languages

Sijacom is available in the following languages which are available after the installation: 

![Übersetzungsstatus](https://weblate.bubu1.eu/widgets/sijacom/-/sijacom/multi-auto.svg)

Consider [translating](https://codeberg.org/pixelcode/Sijacom/src/branch/master/TRANSLATE.md) Sijacom into more languages!

![](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/src/logo/neon-banner.png)

# How Sijacom works

Sijacom is a tiny application and operating it is not particularly hard. You just need to know what JSON is and how HTML tags work. That's all. Here's the concept: Imagine you have a website with 10 sub-pages that have common elements like a header or a footer. What if you want to change the footer's navigation links? You'd have to do the same change to all 10 sub-pages.

Now, Sijacom helps you with that: You don't put the footer's whole HTML code into all the 10 sub-pages but just a "footer" keyword referring to one single "insert file" where the footer is stored. If you want to change it, you just have to do it once in the insert file and not on each of the 10 pages. Then, you just quickly run Sijacom to re-build your website and you're ready to upload the update to your server.

## Root file

First of all, you need a root file. It acts as the central storage of all information needed by Sijacom to build your static websites from different parts. It's basically a JSON file but you need to change the file type to .sijacom because it's not only cool but also the only file type Sijacom accepts.

Have a look at the contents.sijacom file in the `example` folder. It's a real-world root file taken from my [PhishWarn](https://codeberg.org/pixelcode/phishwarn) project. You'll notice that there are four JSON blocks in it:

- general-info (contains general information about the website)
- pages (contains the information of all sub pages of the website)
- general-tags (contains universal tags that appear on multiple or all pages)
- replace (contains information about which codes are to be replaced by which HTML template files)

## Input file

The input file (which is converted to an output file by Sijacom) only consists of content that is unique to it. Any elements that appear on other pages too are replaced by certain keywords which Sijacom looks for. Depending on what you specify in your root file those keywords are replaced by strings or other files' contents.

All input files need to be put in the `input` folder in the root file's directory. The example does not contain all input files specified in the example root file, but just an example input file.

## Insert file

Sijacom looks for keywords and replaces them with the contents of those insert files specified in the root file. For example, you would put a "footer" keyword into every input file and put the actual footer into a dedicated insert file whose name you need to enter into the root file. Then, Sijacom will find the "footer" keyword, go to the footer insert file, take its content and put it where the "footer" keyword is.

All insert files need to be put in the `insert` folder in the root file's directory. The example does not contain all insert files specified in the example root file, but just an example insert file.

# Get started with Sijacom

Refer to the [Wiki](https://codeberg.org/pixelcode/Sijacom/wiki/Get-started-with-Sijacom) to get started with Sijacom.

# CONTRIBUTION

I'm happy about any feedback, recommendations, contributions etc. Just open a new issue and I'll have a look. ☺️

# Licence

![](https://codeberg.org/pixelcode/for-good-eyes-only/raw/branch/master/banner/Banner%20250.png)

Sijacom is licensed under the [For Good Eyes Only Licence v0.2](https://codeberg.org/pixelcode/Sijacom/raw/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf). Don't re-use the title "Sijacom" and its logos and icons.

All versions of Sijacom are licensed under v0.2, which replaces v0.1 under which Sijacom was previously licensed. v0.1 is therefore void for any versions of Sijacom.

Thanks to [Fatcow](https://fatcow.com/free-icons) for providing the awesome in-app icons used by Sijacom under the Creative Commons Attribution 3.0 Licence!

Thanks to [Inno Setup](https://jrsoftware.org/isinfo.php) for providing the awesome installation wizard used by Sijacom for free!

Thanks to [Skitterphoto](https://pixabay.com/de/photos/ziegel-mauer-alt-dunkel-2906556/) for providing the brick wall photo (used in the Sijacom neon banner) under the Pixabay licence!