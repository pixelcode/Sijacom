��    G      T  a   �        �                  !  	   ?     I     V  	   c  #   m  %   �     �     �     �     �     �  	               
     0   '  (   X  +   �     �     �     �     �     �     �     �     �     		     	     '	     <	  	   C	     M	     T	     Y	     b	     i	     y	  	   }	     �	  	   �	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     
  
   
     
     
  	   &
     0
     8
     @
     ]
     j
  4   w
     �
  4   �
  ,   �
  1   %  )   W     �     �  {  �  ,  "     O     Q  5   e     �     �     �     �  $   �  &        9     A     ^     x     �     �  	   �     �     �  3   �  4      5   5     k     x     �     �     �     �     �  -   �  
   �     �            	   &  	   0     :     A     H     O     k  	   s     }     �     �     �  	   �     �     �  	   �            
   +     6     I  	   Q     [     d     l     u     �     �  >   �     �     �     	               -     3        3                   @       9       2   F   G         6   /      ?   ,   A           
   D   '          8      >   	       +            :   1         %              "   &           <   )           C                         .       4         ;   #                  5   =   E   0      (                       B              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-30 16:01+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: French <https://weblate.bubu1.eu/projects/sijacom/sijacom/fr/>
Language: fr
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" est un 
simple générateur de site web statique dont le but est 
de simplifier le processus d'ajustement des éléments 
statiques d'un site web sur plusieurs sous-pages. 
Veuillez vous référer au manuel pour informations sur 
le fonctionnement de Sijacom.
 , Au sujet de Sijacom Tous les fichiers ont été construits avec succès ! Construire tous Fichiers de construction : Des fichiers de construction ? Bulgare C:\documents\MonDrôleProjet\output\ C:\documents\MonDrôleProjet\root.json Annuler Choisir un dossier de sortie Choisissez un répertoire Choisir le dossier de sortie ? Fermer Compilateur : Confirmer Danois Développeur : Voulez-vous vraiment choisir ce dossier de sortie ? Voulez-vous construire les fichiers sélectionnés ? Voulez-vous charger le fichier racine sélectionné ? Néerlandais Anglais Erreur Estonien Fatcow Icônes Fichier Fichier non existant Le type de fichier n'est pas pris en charge ! Finlandais Dossier inexistant Free Pascal Compiler Allemand Islandais Icônes : Gardez Langue LiJu09 Charger le fichier racine ? Journal Mondstern Aucun fichier sélectionné ! Ouvrir un dossier Dossier de sortie sélectionné Dossier de sortie : Pixelcode Polonais Rep. Remplacer Fichier racine chargé Fichier racine : SOS arrêt Sélectionner tout Sijacom Slovaquie Slovène Succès Suédois Traduire Sijacom sur Weblate Traducteurs : Désélectionner tout Vous devez choisir un fichier racine et un dossier de sortie ! Infos Choisissez... Choisissez... Choisir Sélectionnez Infos v1.1 