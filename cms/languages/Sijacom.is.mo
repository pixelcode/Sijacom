��    >        S   �      H  �   I     I     K     Y  	   w     �     �  #   �  %   �     �     �               +  	   1     ;  
   C  0   N  (     +   �     �     �     �     �     �     �               -     B     I     P     U     ^     e     u  	   y     �  	   �     �     �  	   �     �     �     �     �  
   �     �  
   	     	     	     "	     *	  4   7	     l	  4   �	  ,   �	  1   �	  )   
     A
     a
  �  f
  +  �     '  
   )  /   4     d     p       )   �  +   �     �     �     	          *     /  
   ;     F  0   R     �     �  
   �     �     �  
   �     �     �     �               ,     3     :  	   B     L     S     g  	   k     u  
   �     �     �  	   �     �     �  	   �     �     �  	   �  
   �     
  
             &  9   6     p  	   }  	   �     �     �     �     �                $       "   +   ;       &   2      
             ,   *                '         8      9   	                 :       1   %          4         (             7       )   /                           6   0       3                 <   =   >   !   #          -   5             .                  "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-27 16:09+0100
Last-Translator: pixelcode <pixelcode@dismail.de>
Language-Team: Icelandic <https://weblate.bubu1.eu/projects/sijacom/sijacom/is/>
Language: is
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n % 10 != 1 || n % 100 == 11;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 „Einföld tenging og innihaldsstjórnun“ er einföld
truflanir vefsíðuframleiðandi sem hefur það að markmiði 
að einfalda ferli til að laga truflanir vefsíðna þætti yfir
margar undirsíður. Vinsamlegast vísa til handbókarinnar 
fyrir upplýsingar um hvernig á að reka Sijacom.
 , Um Sijacom Allar skrár smíðaðar með góðum árangri! Byggja allt Byggja skrár: Byggja skrár? C:\documents\FyndnaVerkefniðMitt\output\ C:\documents\FyndnaVerkefniðMitt\root.json Hætta við Veldu framleiðslumöppu Veldu möppu Veldu úttaksmappa? Loka Þýðandi: Staðfesta Hönnuður: Viltu virkilega velja þessa framleiðslumöppu? Viltu smíða valdar skrár? Viltu hlaða völdu rótinni? Hollenskur Enska Villa Eistneskur Fatcow tákn Skrá Skrá er ekki til Mappa er ekki til Free Pascal Compiler Pýska Tákn: Geymið Tungumál LiJu09 Hlaða rótarskrá? Lei Mondstern Engin skrá valin! Opna skrá Úttaksmappa valin Úttaksmappa: Pixelcode Pólsku Til. Skipta um Rótaskrá hlaðin Rótarskrá: SOS stopp Velja allt Sijacom Slóvakíu Árangur Afturkalla allt Þú verður að velja rótarskrá og framleiðslumöppu! Upplýsingar Veldu ... Veldu ... Veldu Veldu Upplýsingar v1.1 