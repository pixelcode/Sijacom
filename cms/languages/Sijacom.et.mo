��    :      �  O   �      �  �   �     �       	   %     /     <  #   I  %   m     �     �     �     �     �  	   �     �  
   �  0   �  (   -  +   V     �     �     �     �     �     �     �     �     �     �     �     �                       	   2     <     S  	   b     l     s     x     �  
   �     �  
   �     �     �     �     �  4   �     		  4    	  ,   U	  1   �	  )   �	     �	  ~  �	  *  }     �     �     �     �     �  )     +   ,     X     a     v     �     �  	   �  	   �  	   �  2   �  #   �  &        <     E     M     R     X     g     m     �     �     �     �     �     �     �     �     �  
   �     �       	   !     +     1     8     A  	   T  	   ^     h     u  	   }     �     �  +   �     �     �     �     �     �     �     $      3          *             -   :      7   6   /   %   '      #                                  4       !   "                 5      1      &                           (          +   ,          .   9   0                 )   
       	   2                                  8        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 About Sijacom All files built successfully! Build all Build files: Build files? C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist Folder does not exist Free Pascal Compiler German Icons: Keep Language Load root file? Log No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo PO-Revision-Date: 2021-08-26 15:00+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Estonian <https://weblate.bubu1.eu/projects/sijacom/sijacom/et/>
Language: et
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" on lihtne 
ühendamine ja COntent Management staatiline veebilehe 
generaator, mille eesmärk on lihtsustada staatiliste 
veebisaitide elementide kohandamise protsessi kogu
mitme alamlehe vahel. Palun vaadake käsiraamatut 
teavet Sijacomi kasutamise kohta.
 Kohta Sijacomi Kõik failid ehitatud edukalt! Ehita kõik Failide loomine: Ehitada faile? C:\dokumendid\MinuNaljakasProjekt\output\ C:\dokumendid\MinuNaljakasProjekt\root.json Tühista Valige väljundkaust Valige kataloog Vali väljundkaust? Sulge Koostaja: Kinnitage Arendaja: Kas te tõesti soovite seda väljundkausta valida? Kas soovite valitud faile koostada? Kas soovite valitud juurfaili laadida? Hollandi Inglise Viga Eesti Fatcow ikoonid Faili Faili ei ole olemas Kausta ei ole olemas Free Pascal Compiler Saksa Ikoonid: Hoida Keel Laadige juurfaili? Logi Faili pole valitud! Avage fail Väljundkaust valitud Väljundkaust: Pixelcode Poola Viide. Asendage Juurfaili laaditud Juurfail: SOS stopp Valige kõik Sijacom Slovakkia Edukus Tühista kõik Sa pead valima juurfaili ja väljundkausta! Infot Vali... Vali... Valige Valige Infot 