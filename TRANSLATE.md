# Translate

Feel free to translate Sijacom into any language on the [Weblate](https://weblate.bubu1.eu/engage/sijacom) server of @bubu!

## Status

![Übersetzungsstatus](https://weblate.bubu1.eu/widgets/sijacom/-/sijacom/multi-auto.svg)

## Translators
Many, many thanks to the translators who made and make Sijacom accessible in so many languages!

- Translator-in-chief (all languages): @mondstern
- Slovak: [LiJu09](https://weblate.bubu1.eu/user/LiJu09)

## Licence

Even though I have chosen the Hippocratic Licence 2.1 for the translation strings, those are so simply that they don't reach the necessary level of artistic creation to be protectable by copyright, therefore they are actually in the public domain. That's why you don't need to worry about copyright issues regarding the translation at all!