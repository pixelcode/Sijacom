��    I      d  a   �      0  �   1     1     3     A  	   _     i     v  	   �  #   �  %   �     �     �     �            	   #     -     5  
   <  0   G  (   x  +   �     �     �     �     �     �     �     �     	     )	     1	     G	     \	  	   c	  	   m	     w	     ~	     �	     �	     �	     �	  	   �	     �	  	   �	     �	     �	  	   �	     �	     
     	
     
     
  
   +
     6
  
   ?
     J
     R
  	   Y
     c
     k
     s
     �
     �
  4   �
     �
  4   �
  ,   +  1   X  )   �     �     �  �  �  S  �     �     �  .   �          .     F     \  (   e  *   �  	   �     �     �     �          "     /     ;     C  3   P  -   �  8   �  	   �     �     �  
                  &  "   ;     ^     j     |     �  	   �  
   �     �  
   �     �     �      �     �  	   �               .     H  	   [  	   e     o     t     �     �     �  
   �     �     �     �     �     �     �               *  D   ;  
   �     �     �  
   �  
   �  
   �     �        4                   B       ;       3   $   I         8   0      A   -   C           
   F   (          :      @   	       ,            <   2         &              "   '           >   *           E                         /   H   6         =   #                  7   ?   G   1      )                       D          5   .   !   %   +          9        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Hungarian Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Romanian Root file loaded Root file: SOS stop Select all Sijacom Slovak Slovenian Success Swedish Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-29 15:14+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Romanian <https://weblate.bubu1.eu/projects/sijacom/sijacom/ro/>
Language: ro
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" este o 
simplă aplicație de generator static de site-uri web al 
cărui scop este de a simplifica procesul de procesului 
de ajustare a elementelor statice ale site-ului web în 
mai multe subpagini. Vă rugăm să consultați manualul 
pentru informații privind modul de funcționare a Sijacom.
 , Despre Sijacom Toate fișierele au fost construite cu succes! Construiți totul Construiți fișierele: Construiți fișiere? Bulgară C:\documente\ProiectulMeuAmuzant\output} C:\documente\ProiectulMeuAmuzant\root.json Anulează Selectați un dosar de ieșire Selectați directorul Alegeți folderul de ieșire? Închideți Compilator:: Confirmați Daneză Dezvoltator: Chiar doriți să alegeți acest folder de ieșire? Doriți să construiți fișierele selectate? Doriți să încărcați fișierul rădăcină selectat? Olandeză Engleză Eroare Estoniană Fatcow Icoane Fișier Fișierul nu există Tipul de fișier nu este acceptat! Finlandeză Folder nu există Free Pascal Compiler Germană Maghiară Islandeză Icoane: Păstrați Limba LiJu09 Încarcă fișierul rădăcină? Logare Mondstern Nici un fișier selectat! Deschidere fișier Dosar de ieșire selectat Folder de ieșire: Pixelcode Poloneză Rif. Înlocuiește Român Fișier rădăcină încărcat Fișier rădăcină: SOS oprire Selectați tot Sijacom Slovacă Slovenă Reușita Suedeză Tradu Sijacom pe Weblate Traducători: Deosebiți toate Trebuie să alegeți un fișier rădăcină și un dosar de ieșire! Informatii Alegeți... Alegeți... Selectați Selectați Informatii v1.1 