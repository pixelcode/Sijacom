unit uInfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, uTranslationStrings,
  LCLTranslator, StdCtrls,lclintf;

type

  { TInfoForm }

  TInfoForm = class(TForm)
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticTextVersion: TStaticText;
    StaticText6: TStaticText;
    StaticText7: TStaticText;
    StaticText8: TStaticText;
    StaticTxtFatcow1: TStaticText;
    StaticTxtMondstern: TStaticText;
    StaticTxtLiJu09: TStaticText;
    StaticTxtTranslate: TStaticText;
    SttcTxtFPC: TStaticText;
    StaticText5: TStaticText;
    StaticTxtFatcow: TStaticText;
    procedure StaticTxtFatcow1Click(Sender: TObject);
    procedure StaticTxtLiJu09Click(Sender: TObject);
    procedure StaticTxtMondsternClick(Sender: TObject);
    procedure StaticTxtTranslateClick(Sender: TObject);
    procedure SttcTxtFPCClick(Sender: TObject);
    procedure StaticTxtFatcowClick(Sender: TObject);
  private

  public

  end;

var
  InfoForm: TInfoForm;

implementation

{$R *.lfm}

{ TInfoForm }

procedure TInfoForm.StaticTxtFatcowClick(Sender: TObject);
begin
  OpenURL('https://fatcow.com/free-icons');
end;

procedure TInfoForm.SttcTxtFPCClick(Sender: TObject);
begin
  OpenURL('https://www.freepascal.org/');
end;

procedure TInfoForm.StaticTxtFatcow1Click(Sender: TObject);
begin
  OpenURL('https://codeberg.org/pixelcode');
end;

procedure TInfoForm.StaticTxtLiJu09Click(Sender: TObject);
begin
  OpenURL('https://weblate.bubu1.eu/user/LiJu09');
end;

procedure TInfoForm.StaticTxtMondsternClick(Sender: TObject);
begin
  OpenURL('https://weblate.bubu1.eu/user/mondstern');
end;

procedure TInfoForm.StaticTxtTranslateClick(Sender: TObject);
begin
  OpenURL('https://weblate.bubu1.eu/engage/sijacom');
end;

end.

