��    E      D  a   l      �  �   �     �     �       	        )     6  	   C  #   M  %   q     �     �     �     �     �  	   �     �     �  
   �  0     (   8  +   a     �     �     �     �     �     �     �     �     �     �     	     	  	   #	     -	     4	     9	     B	     I	     Y	  	   ]	     g	  	   y	     �	     �	  	   �	     �	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     
     
     +
     8
  4   E
     z
  4   �
  ,   �
  1   �
  )   %     O     o  �  t  =  (     f     h  '   {     �     �     �  	   �  &   �  (     	   4     >     T     d     {     �     �     �     �  (   �  &   �  0   �  
   ,     7     D     K     T     a     j     ~     �     �     �     �  	   �     �     �     �     �     �       	             8     I     ^  	   l     v     ~  
   �     �     �     �  
   �     �  	   �     �     �       
     1   %     W     c     o     {     �     �     �        3                   >       9       2   D   E         6   /      =   ,   ?           
   B   '          8      <   	       +                1         %              "   &               )           A                         .       4         :   #                  5   ;   C   0      (                       @              -   !   $   *          7        "Simple Joining And COntent Management" is a simple 
static website generator whose purpose is to simplify the 
process of adjusting static website elements across
multiple sub-pages. Please refer to the manual for 
information on how to operate Sijacom.
 , About Sijacom All files built successfully! Build all Build files: Build files? Bulgarian C:\documents\MyFunnyProject\output\ C:\documents\MyFunnyProject\root.json Cancel Choose an output folder Choose directory Choose output folder? Close Compiler: Confirm Danish Developer: Do you really want to choose this output folder? Do you want to build the selected files? Do you want to load the selected root file? Dutch English Error Estonian Fatcow Icons File File does not exist File type not supported! Finnish Folder does not exist Free Pascal Compiler German Icelandic Icons: Keep Language LiJu09 Load root file? Log Mondstern No file selected! Open file Output folder selected Output folder: Pixelcode Polish Ref. Replace Root file loaded Root file: SOS stop Select all Sijacom Slovak Success Translate Sijacom on Weblate Translators: Unselect all You have to choose a root file and an output folder! tinfoform.captionInfo tmainform.bitbtnchooseoutputfolder.captionChoose... tmainform.bitbtnchooseroot.captionChoose... tmainform.bitbtnselectoutputfolder.captionSelect tmainform.bitbtnselectroot.captionSelect tmainform.menuinfo.captionInfo v1.1 PO-Revision-Date: 2021-08-28 15:59+0100
Last-Translator: mondstern <mondstern@snopyta.org>
Language-Team: Slovenian <https://weblate.bubu1.eu/projects/sijacom/sijacom/sl/>
Language: sl
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3;
X-Generator: Poedit 1.5.7
Project-Id-Version: 
POT-Creation-Date: 
MIME-Version: 1.0
 "Simple Joining And COntent Management" je preprost 
program za statični generator spletnih strani, katerega 
namen je poenostaviti postopek prilagajanja statičnih 
elementov spletnih strani v vseh državah članicah. več 
podstraneh. Oglejte si priročnik za informacije o tem, 
kako uporabljati program Sijacom.
 , O podjetju Sijacom Vse datoteke so bile uspešno vgrajene! Zgradite vse Datoteke za sestavo: Datoteke za gradnjo? Bolgarski C:\dokumenti\MojSmešniProjekt\output\ C:\dokumenti\MojSmešniProjekt\root.json Prekliči Izberite izhodno mapo Izberite imenik Izberite izhodno mapo? Zapri Sestavljavec: Potrdite Danska Razvijalec: Ali res želite izbrati to izhodno mapo? Ali želite izdelati izbrane datoteke? Ali želite naložiti izbrano korensko datoteko? Nizozemski Angleščina Napaka Estonski Fatcow Ikone Datoteka Datoteka ne obstaja Tip datoteke ni podprt! Finska Mapa ne obstaja Free Pascal Compiler Nemški Islandski Ikone: Ohranite Jezik LiJu09 Naloži korensko datoteko? Zalog Mondstern Ni izbrana nobena datoteka! Odprite datoteko Izbrana izhodna mapa Izhodne mape: Pixelcode Poljski Skl. Zamenjajte Korenska datoteka je naložena Koreninska datoteka: SOS postanek Izberi vse Sijacom Slovaška Uspeh Prevesti Sijacom na Weblate Prevajalci: Izberi vse Izbrati morate korensko datoteko in izhodno mapo! Informacije Izberite... Izberite... Izberite Izberite Informacije v1.1 